%% Montecarlo simulation 
clc
clear
close all

iter = 10000; % number of iterations

%% Graphical User Interface (GUI)
list = {'Create a new cell','Use an existing cell (montecarlo.mat)','Read excel file (montecarlo.xlsm)'};
input = listdlg('PromptString',{'Data input.'},...
    'SelectionMode','single','ListSize',[300,100],'ListString',list);

%% Election from the GUI code to get montecarlo cell
%
%       input = 1 ; Manual input of the data
%       input = 2 ; Read previous montecarlo.mat file data
%       input = 3 ; Read montecarlo.xlsm file
%

if input == 1 % Manual imput of the data
    prompt = {'Number of elements'};
    dlgtitle = 'Data input ';
    dims = [1 40];
    definput = {'0'};
    n = inputdlg(prompt,dlgtitle,dims,definput);
    n = str2double(n);
    montecarlo = cell(n,5);
    
    msgbox({'Enter first critical risks in order of priority.';'Possible types of risk:';...
        '  1. Critical risk (CRIT)';'  2. Normal risk (RISK)'},...
        'Info','help')
    
    for i = 1:n
        prompt = {'Name of the risk','Potential Cost [€]','Risk probability [%]','Type of risk (RISK or CRIT)'};
        dlgtitle = (['Element ',num2str(i),' of',num2str(n)]);
        dims = [1 40; 1 40; 1 40; 1 40];
        definput = {'','0','0','Risk'};
        answer = inputdlg(prompt,dlgtitle,dims,definput);
        montecarlo{i,1} = i;
        montecarlo{i,2} = answer{1,1};
        montecarlo{i,3} = str2double(answer{2,1});
        montecarlo{i,4} = str2double(answer{3,1});
        montecarlo{i,5} = answer{4,1};
    end
    save('montecarlo.mat','montecarlo')
    
elseif input==2 % Read previous montecarlo.mat file data
    load montecarlo.mat
    [n,~] = size(montecarlo);
    
else % input ==3 ; Read montecarlo.xlsm file
    filename='montecarlo.xlsm';
    sheet=2;
    xlRange='A2:F11';
    [xa, xb] = xlsread(filename,sheet,xlRange);
    xb(:,3)=num2cell(xa(:,1));
    xb(:,4)=num2cell(xa(:,2));
    montecarlo=xb;
    
    n=length(montecarlo);
    % save('montecarlo.mat','montecarlo') 
   
end


%% Montecarlo simulation code
values = zeros(n,2);
for i = 1:n
    values(i,1) = montecarlo{i,3};
    values(i,2) = montecarlo{i,4};
    if strcmp(montecarlo{i,5},'CRIT') == 1
        c = i;
    end
end

res1 = zeros(iter,n+1);
res2 = zeros(iter,2);
for i = 1:iter
    random = rand(n,1)*100;
    res1(i,1:n) = random(:,1) < values(:,2);
    
    for k = 1:c
        if res1(i,k) == 1
            res1(i,k+1:end-1) = 0;
        end
    end
    res1(i,n+1) = dot(res1(i,1:n), values(:,1)); % Needs to go after every 0 if CRIT triggers

end

res2(:,1) = linspace(100/iter,100,iter);
b = sortrows(res1,n+1);
res2(:,2) = b(:,n+1);

figure(1)
title('Montecarlo overcosts beyond Cost Estimate')
hold on
plot(res2(:,1),res2(:,2),'-','Color',[0, 0.4470, 0.7410],'LineWidth',2)
ylabel('Contingency','fontsize',12); ytickformat('eur')
xlabel('Confidence','fontsize',12); xtickformat('percentage')
set(gca,'fontsize',12,'box','on')

filename = 'montecarlo.xlsm';
writecell(montecarlo,filename,'Sheet',2,'Range','A2')
